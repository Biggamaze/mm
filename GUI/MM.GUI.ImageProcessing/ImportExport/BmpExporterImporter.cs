﻿using MM.SimpleCellularAutomata;
using System;
using System.IO;
using System.Windows.Media.Imaging;

namespace MM.GUI.ImageProcessing.ImportExport
{
    public static class BmpExporterImporter
    {
        public static void SaveToFile(string filename, Cell[][] cells)
        {
            var image = GrainArrayProcessor.ToImageSource(cells, cell => cell.Id);
            if (filename != string.Empty)
            {
                using (FileStream stream5 = new FileStream(filename, FileMode.Create))
                {
                    PngBitmapEncoder encoder5 = new PngBitmapEncoder();
                    encoder5.Frames.Add(BitmapFrame.Create(image));
                    encoder5.Save(stream5);
                }
            }
        }

        public static int[][] ReadFromFile(string path)
        {
            BitmapImage bitmap = new BitmapImage(new Uri(path, UriKind.Absolute));
            return GrainArrayProcessor.FromImageSource(new WriteableBitmap(bitmap));
        }
    }
}
