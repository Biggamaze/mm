﻿using MM.SimpleCellularAutomata;
using System;
using System.IO;
using System.Text;

namespace MM.GUI.ImageProcessing.ImportExport
{
    public static class TextExporterImporter
    {
        public static void ExportToTxt(Cell[][] cells, string path) 
        {
            var builder = new StringBuilder();

            var rows = cells.Length;
            var columns = cells[0].Length;

            builder.AppendLine($"{rows} {columns} 1");

            for (int r = 0; r < columns; ++r)
            {
                for (int c = 0; c < rows; ++c)
                {
                    if (cells[r][c].State == CellState.Excluded)
                    {
                        builder.AppendLine($"{r} {c} -1 {cells[r][c]}");
                    }
                    else
                    {
                        builder.AppendLine($"{r} {c} 0 {cells[r][c]}");
                    }
                }
            }

            File.WriteAllText(path, builder.ToString());
        }

        public static int[][] ImportFromTxt(string path)
        {
            var lines = File.ReadAllLines(path);
            var firstLine = Array.ConvertAll(lines[0].Split(' '), el => int.Parse(el));

            var arr = new int[firstLine[0]][];

            for (int i = 0; i < firstLine[0]; ++i)
            {
                arr[i] = new int[firstLine[1]];
            }

            for (int i = 1; i < lines.Length; ++i)
            {
                var line = Array.ConvertAll(lines[i].Split(' '), el => int.Parse(el));
                arr[line[0]][line[1]] = line[3];
            }

            return arr;
        }
    }
}
