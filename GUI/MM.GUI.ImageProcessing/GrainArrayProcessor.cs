﻿using MM.GUI.ImageProcessing.Color;
using MM.SimpleCellularAutomata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace MM.GUI.ImageProcessing
{
    public class GrainArrayProcessor
    {
        public static WriteableBitmap ToImageSource(Cell[][] cells, Func<Cell, int> fieldSelector)
        {
            var cellsnum = cells.Select(x => x.Select(fieldSelector).ToArray()).ToArray();
            var rows = cells.Length; // width before
            var columns = cells[0].Length; // height before

            var wbitmap = new WriteableBitmap(columns, rows, 96, 96, PixelFormats.Bgr32, null);
            byte[,,] pixels = new byte[rows, columns, 4];

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < columns; c++)
                {
                    var a = ColorPalette.GetRgb(cellsnum[r][c]);
                    for (int k = 0; k < 3; k++)
                    {
                        pixels[r, c, k] = a[k];
                    }
                }
            }

            byte[] pixels1d = new byte[rows * columns * 4];
            int index = 0;
            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < columns; col++)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        pixels1d[index++] = pixels[row, col, i];
                    }
                }
            }

            Int32Rect rect = new Int32Rect(0, 0, columns, rows);
            int stride = 4 * columns;
            wbitmap.WritePixels(rect, pixels1d, stride, 0);

            return wbitmap;
        }

        public static int[][] FromImageSource(BitmapSource source)
        {
            if (source.Format != PixelFormats.Bgr32)
            {
                source = new FormatConvertedBitmap(source, PixelFormats.Bgr32, null, 0);
            }

            var columns = source.PixelWidth;
            var rows = source.PixelHeight;
            var result = new int[rows][];

            for (int i = 0; i < rows; ++i)
            {
                result[i] = new int[columns];
            }

            CopyPixels(source, result, columns * 4, 0);
            return result;
        }

        public static void CopyPixels(BitmapSource source, int[][] pixels, int stride, int offset)
        {
            var dict = new Dictionary<GrainColor, int>(new GrainColorEqualityComparer());
            var height = source.PixelHeight;
            var width = source.PixelWidth;
            var pixelBytes = new byte[height * width * 4];
            source.CopyPixels(pixelBytes, stride, 0);
            int r0 = offset / width;
            int c0 = offset - width * r0;
            for (int r = 0; r < height; r++)
            {
                for (int c = 0; c < width; c++)
                {
                    var color = new GrainColor()
                    {
                        Blue = pixelBytes[(r * width + c) * 4 + 0],
                        Green = pixelBytes[(r * width + c) * 4 + 1],
                        Red = pixelBytes[(r * width + c) * 4 + 2],
                        Opacity = pixelBytes[(r * width + c) * 4 + 3],
                    };

                    pixels[r + r0][c + c0] = ColorPalette.GetValue((color.Red, color.Green, color.Blue));
                };
            }
        }
    }
}

