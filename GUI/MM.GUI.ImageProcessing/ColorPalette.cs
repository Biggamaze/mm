﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MM.GUI.ImageProcessing
{
    public static class ColorPalette
    {
        private static Random random = new Random();

        private static Dictionary<int, byte[]> valueToRgb = new Dictionary<int, byte[]>
        {
            {-1, new byte[] {0,0,0,0} },
            {0, new byte[] {255,255,255,255} }
        };

        private static Dictionary<(byte red, byte green, byte blue), int> rgbToVal = new Dictionary<(byte red, byte green, byte blue), int>
        {
            {(0,0,0), -1},
            {(255,255,255), 0 }
        };

        public static byte[] GetRgb(int value)
        {
            if (!valueToRgb.ContainsKey(value))
            {
                var bgr = new byte[4];

                while (true)
                {
                    if (value >= int.MaxValue / 2)
                    {
                        // recrystalized
                        bgr[2] = (byte)random.Next(90, 256);
                        bgr[1] = (byte)random.Next(0, 20);
                        bgr[0] = (byte)random.Next(0, 20);
                    }
                    else
                    {
                        // normal grains
                        bgr[2] = (byte)random.Next(0, 100);
                        bgr[1] = (byte)random.Next(40, 150);
                        bgr[0] = (byte)random.Next(90, 256);
                    }

                    bgr[3] = 255;

                    if (rgbToVal.ContainsKey((bgr[0], bgr[1], bgr[2])))
                    {
                        continue;
                    }

                    valueToRgb.Add(value, bgr);
                    rgbToVal.Add((bgr[0], bgr[1], bgr[2]), value);
                    break;
                }
            }

            return valueToRgb[value];
        }

        public static int GetValue((byte red, byte green, byte blue) rgb)
        {
            if (!rgbToVal.ContainsKey(rgb))
            {
                var rgbarr = new byte[4] { rgb.blue, rgb.green, rgb.red, 0 };
                var maxV = valueToRgb.Keys.Max();

                valueToRgb.Add(maxV + 1, rgbarr);
                rgbToVal.Add(rgb, maxV + 1);
            }

            return rgbToVal[rgb];
        }
    }
}
