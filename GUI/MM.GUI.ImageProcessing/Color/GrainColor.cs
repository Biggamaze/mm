﻿namespace MM.GUI.ImageProcessing.Color
{
    public struct GrainColor 
    {
        public byte Red { get; set; }
        public byte Green { get; set; }
        public byte Blue { get; set; }
        public byte Opacity { get; set; }
    }
}
