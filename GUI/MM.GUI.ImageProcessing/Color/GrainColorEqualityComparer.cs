﻿using System.Collections.Generic;

namespace MM.GUI.ImageProcessing.Color
{
    public class GrainColorEqualityComparer : IEqualityComparer<GrainColor>
    {
        public bool Equals(GrainColor x, GrainColor y)
        {
            return (x.Blue == y.Blue && x.Green == y.Green && x.Opacity == y.Opacity && x.Red == y.Red);
        }

        public int GetHashCode(GrainColor obj)
        {
            return obj.GetHashCode();
        }
    }
}
