﻿namespace MM.SimpleCellularAutomata
{
    public enum CellState
    {
        Empty,
        Occupied,
        Excluded
    }
}
