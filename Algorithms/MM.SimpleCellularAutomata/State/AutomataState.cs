﻿namespace MM.SimpleCellularAutomata
{
    public enum AutomataState
    {
        Uninitialized,
        JustInitialized,
        Idle,
        Calculating,
        Finished
    }
}
