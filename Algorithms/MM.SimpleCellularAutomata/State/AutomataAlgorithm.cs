﻿namespace MM.SimpleCellularAutomata
{
    public enum AutomataAlgorithm
    {
        None,
        CellularAutomata,
        MonteCarlo,
        Recrystalization
    }
}
