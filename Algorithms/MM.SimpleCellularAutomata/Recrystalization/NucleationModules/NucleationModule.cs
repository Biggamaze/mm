﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MM.SimpleCellularAutomata.Recrystalization.NucleationModules
{
    public abstract class NucleationModule
    {
        protected static Random random = new Random();
        public abstract int Nucleate(List<(int r, int c)> bank, int maxnumber, HashSet<(int r, int c)> recrystalized, Cell[][]cells, int inStep);
        public abstract void Reset();
    }
}
