﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MM.SimpleCellularAutomata.Recrystalization.NucleationModules
{
    public class IncresingNucleationModule : NucleationModule
    {
        private int lastStepAdded;

        public IncresingNucleationModule()
        {
            lastStepAdded = 0;
        }

        public override int Nucleate(List<(int r, int c)> bank, int maxnumber, HashSet<(int r, int c)> recrystalized, Cell[][] cells, int inStep)
        {
            inStep += lastStepAdded;

            if (bank.Count < inStep)
            {
                inStep = bank.Count;
            }

            while (inStep > 0)
            {
                var (r, c) = bank[random.Next(0, bank.Count)];
                bank.Remove((r, c));

                cells[r][c].Id = maxnumber;
                cells[r][c].H = 0;
                maxnumber++;
                recrystalized.Add((r, c));
                inStep--;
            }

            lastStepAdded = inStep;
            return maxnumber;
        }

        public override void Reset()
        {
            lastStepAdded = 0;
        }
    }
}
