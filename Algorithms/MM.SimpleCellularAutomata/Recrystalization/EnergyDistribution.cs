﻿using MM.CalcParameters.Neighbourhood;
using System.Linq;

namespace MM.SimpleCellularAutomata.Recrystalization
{
    public static class EnergyDistribution
    {
        public static void Distribute(Cell[][] cells, int boundaryEnergy, int nonBoundaryEnergy)
        {
            var neighbourhood = new MooreNeigbourhood();
            int rows = cells.Length;
            int columns = cells[0].Length;

            for (int r = 0; r < rows; ++r)
            {
                for (int c = 0; c < columns; ++c)
                {
                    if (cells[r][c].State != CellState.Occupied)
                    {
                        continue;
                    }

                    var neighbours = neighbourhood.GetNeighbours(cells, (r, c)).Where(n => n.State == CellState.Occupied);
                    
                    if (neighbours.Select(cell => cell.Id).Distinct().Count() > 1)
                    {
                        cells[r][c].H = boundaryEnergy;
                    }
                    else
                    {
                        cells[r][c].H = nonBoundaryEnergy;
                    }
                }
            }
        }
    }
}
