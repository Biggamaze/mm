﻿using MM.SimpleCellularAutomata;

namespace MM.CalcParameters.Inclusions
{
    public class RectangularInclusion : Inclusion
    {
        protected override void Put(Cell[][] cells, int radius, (int row, int column) center)
        {
            for (var r = center.row - radius; r < center.row + radius; r++)
            {
                for (var c = center.column - radius; c < center.column + radius; c++)
                {
                    cells[r][c].Id = -1;
                    cells[r][c].State = CellState.Excluded;
                }
            }
        }
    }
}
