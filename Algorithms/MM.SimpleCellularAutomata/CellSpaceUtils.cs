﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MM.SimpleCellularAutomata
{
    internal static class CellSpaceUtils
    {
        public static int[][] CopyCells(this int[][] cells)
        {
            var rows = cells.Length;
            var columns = cells[0].Length;

            int[][] newCells = new int[rows][];
            for (int r = 0; r < rows; ++r)
            {
                newCells[r] = new int[columns];
                for (int c = 0; c < columns; ++c)
                {
                    newCells[r][c] = cells[r][c];
                }
            }

            return newCells;
        }
    }
}
