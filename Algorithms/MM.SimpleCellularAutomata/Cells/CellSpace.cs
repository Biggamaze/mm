﻿using MM.CalcParameters.Inclusions;
using MM.SimpleCellularAutomata.Algorithm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MM.SimpleCellularAutomata
{
    public class CellSpace
    {
        private static readonly Random random = new Random();
        private int maximumNumber;

        public Recrystalizer Recrystalization { get; set; }
        public int Rows { get; }
        public int Columns { get; }
        public Cell[][] Cells { get; private set; }
        public AutomataState State { get; private set; }
        public AutomataAlgorithm CurrentAlgorithm { get; set; }
        public int Rule4Probability { get; set; }
        public double GrainBoundaryEnergy { get; set; }

        private CellSpace()
        {
            maximumNumber = 1;
        }

        public CellSpace(int rows, int columns) : this()
        {
            State = AutomataState.Uninitialized;
            Rows = rows;
            Columns = columns;
            InitializeCells();
            State = AutomataState.Idle;
        }

        public CellSpace(int[][] cells) : this()
        {
            State = AutomataState.Uninitialized;
            Rows = cells.Length;
            Columns = cells[0].Length;
            InitializeCells(cells);
            State = AutomataState.Idle;
        }

        private void InitializeCells()
        {
            Cells = new Cell[Rows][];
            for (int r = 0; r < Rows; ++r)
            {
                Cells[r] = new Cell[Columns];
                for (int c = 0; c < Columns; ++c)
                {
                    Cells[r][c] = new Cell { Id = 0, H = 0, State = CellState.Empty };
                }
            }
            CurrentAlgorithm = AutomataAlgorithm.None;
        }

        private void InitializeCells(int[][] initialState)
        {
            Cells = new Cell[Rows][];

            var isAnyEmpty = false;

            for (int r = 0; r < Rows; ++r)
            {
                Cells[r] = new Cell[Columns];
                for (int c = 0; c < Columns; ++c)
                {
                    var val = initialState[r][c];

                    if (val == 0)
                    {
                        isAnyEmpty = true;
                    }

                    CellState state = val > 0 ? CellState.Occupied : (val == 0 ? CellState.Empty : CellState.Excluded);
                    Cells[r][c] = new Cell { Id = initialState[r][c], H = 0, State = state };
                }
            }

            switch (isAnyEmpty)
            {
                case true:
                    CurrentAlgorithm = AutomataAlgorithm.CellularAutomata;
                    break;
                case false:
                    CurrentAlgorithm = AutomataAlgorithm.MonteCarlo;
                    break;
            }
        }

        public void PutCellsCA(int count)
        {
            CellularAutomata.PutCells(Cells, maximumNumber, count);
            maximumNumber += count;
            CurrentAlgorithm = AutomataAlgorithm.CellularAutomata;
        }

        public void PutCellsMC(int numStates)
        {
            MonteCarlo.Put(Cells, maximumNumber, numStates);
            maximumNumber += numStates;
            CurrentAlgorithm = AutomataAlgorithm.MonteCarlo;
        }

        public void Generations(int count)
        {
            State = AutomataState.Calculating;
            switch (CurrentAlgorithm)
            {
                case AutomataAlgorithm.CellularAutomata:
                    var changes = CellularAutomata.Generations(Cells, Rule4Probability, count);
                    if (changes == 0)
                    {
                        State = AutomataState.Finished;
                    }
                    return;
                case AutomataAlgorithm.MonteCarlo:
                    MonteCarlo.Generations(Cells, count);
                    break;
                case AutomataAlgorithm.Recrystalization:
                    if (Recrystalization == null)
                    {
                        return;
                    }
                    Recrystalization.Generations(count);
                    break;
                default:
                    break;
            }
            State = AutomataState.Idle;
        }

        public Cell[][] FinalGeneration()
        {
            if (State != AutomataState.Idle)
            {
                return Cells;
            }

            while (State != AutomataState.Finished)
            {
                Generations(1);
            }

            return Cells;
        }

        public Cell[][] PutInclusions(int count, int radius, Inclusion inclusion)
        {
            State = AutomataState.Calculating;

            CellularAutomata.PutInclusions(Cells, count, radius, inclusion);

            State = AutomataState.Idle;
            return Cells;
        }

        public void Clean(IEnumerable<int> except)
        {
            for (var r = 0; r < Rows; ++r)
            {
                for (var c = 0; c < Columns; ++c)
                {
                    if (!except.Any(value => value == Cells[r][c].Id))
                    {
                        Cells[r][c].Id = 0;
                        Cells[r][c].State = CellState.Empty;
                    }
                }
            }
        }

        public void SetStructure(Func<int, int> action)
        {
            for (int r = 0; r < Rows; ++r)
            {
                for (int c = 0; c < Columns; ++c)
                {
                    if (Cells[r][c].State == CellState.Occupied)
                    {
                        Cells[r][c].Id = action(Cells[r][c].Id);
                        Cells[r][c].State = CellState.Excluded;
                    }
                }
            }
        }

        public void UnsetStructure()
        {
            for (int r = 0; r < Rows; ++r)
            {
                for (int c = 0; c < Columns; ++c)
                {
                    if (Cells[r][c].Id == -2)
                    {
                        Cells[r][c].Id = ++maximumNumber;
                        Cells[r][c].State = CellState.Occupied;
                    }

                    if(Cells[r][c].Id > 0 && Cells[r][c].State == CellState.Excluded)
                    {
                        Cells[r][c].State = CellState.Occupied;
                    }
                }
            }
        }
    }
}
