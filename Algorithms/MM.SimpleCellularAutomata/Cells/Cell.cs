﻿namespace MM.SimpleCellularAutomata
{
    public struct Cell
    {
        public int Id;
        public int H;
        public CellState State;
    }
}
