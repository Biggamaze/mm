﻿namespace MM.CalcParameters.Neighbourhood
{
    public class VonNeumannNeighbourhood : Neighbourhood
    {
        protected override (int row, int column)[] GetIndices((int row, int column) cds)
        {
            return new[]
            {
                (cds.row - 1, cds.column),
                (cds.row + 1, cds.column),
                (cds.row, cds.column + 1),
                (cds.row, cds.column - 1),
            };
        }
    }
}
