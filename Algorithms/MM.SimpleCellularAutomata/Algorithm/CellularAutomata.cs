﻿using MM.CalcParameters;
using MM.CalcParameters.Inclusions;
using System;
using System.Collections.Generic;

namespace MM.SimpleCellularAutomata.Algorithm
{
    public class CellularAutomata
    {
        private static Random random = new Random();

        public static void PutCells(Cell[][] cells, int maximumNumber, int count)
        {
            var rows = cells.Length;
            var columns = cells[0].Length;

            int i;
            for (i = maximumNumber; i <= maximumNumber + count; ++i)
            {
                int counter = 0;
                do
                {
                    var randRow = random.Next(0, rows);
                    var randCol = random.Next(0, columns);
                    if (cells[randRow][randCol].State != CellState.Empty)
                    {
                        counter++;
                        continue;
                    }

                    cells[randRow][randCol].Id = i;
                    cells[randRow][randCol].State = CellState.Occupied;

                    break;

                } while (true || counter < 300);
            }
        }

        public static void PutInclusions(Cell[][] cells, int count, int radius, Inclusion inclusion)
        {
            var rows = cells.Length;
            var columns = cells[0].Length;
            var usedBorders = new HashSet<(int, int)>();

            var sec = 10;
            while (count > 0 && sec > 0)
            {
                var (r, c) = (random.Next(0, rows - 1), random.Next(0, columns - 1));

                if (cells[r][c].State == CellState.Empty)
                {
                    if (inclusion.TryPut(cells, radius, (r, c)))
                    {
                        count--;
                    }
                    else
                    {
                        sec--;
                    }
                }
                else
                {
                    while (r > 0 && c > 0)
                    {
                        if (usedBorders.Contains((cells[r][c].Id, cells[r + 1][c + 1].Id)))
                        {
                            sec--;
                            continue;
                        }

                        var isBorder = cells[r][c].Id != cells[r + 1][c + 1].Id;
                        if (isBorder && inclusion.TryPut(cells, radius, (r, c)))
                        {
                            var (v1, v2) = (cells[r][c].Id, cells[r + 1][c + 1].Id);
                            usedBorders.Add((v1, v2));
                            usedBorders.Add((v2, v1));
                            count--;
                            break;
                        }
                        r--;
                        c--;
                    }
                }
            }
        }

        public static int Generations(Cell[][] cells, int rule4p, int numGeneations)
        {
            int changes = 1;
            for (int g = 0; g < numGeneations; ++g)
            {
                changes = Generation(cells, rule4p);
            }
            return changes;
        }

        public static int Generation(Cell[][] cells, int rule4p)
        {
            var rows = cells.Length;
            var columns = cells[0].Length;
            var nopass = 0;

            var genChanges = new Dictionary<(int row, int column), int>();
            for (int r = 0; r < rows; ++r)
            {
                for (int c = 0; c < columns; ++c)
                {
                    if (cells[r][c].State != CellState.Empty)
                    {
                        continue;
                    }

                    var newVal = TransitionRules.TranstionRule(cells, r, c, rule4p);

                    if (newVal < 0)
                    {
                        nopass++;
                        continue;
                    }

                    if (newVal != 0)
                    {
                        genChanges.Add((r, c), newVal);
                    }
                }
            }

            foreach (var ch in genChanges)
            {
                cells[ch.Key.row][ch.Key.column].Id = ch.Value;
                cells[ch.Key.row][ch.Key.column].State = CellState.Occupied;
            }

            return genChanges.Count + nopass;
        }
    }
}
