﻿using MM.CalcParameters.Neighbourhood;
using MM.SimpleCellularAutomata.Recrystalization.NucleationModules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MM.SimpleCellularAutomata.Algorithm
{
    public class Recrystalizer
    {
        private static readonly Random rand = new Random();

        private readonly int minnumber = int.MaxValue / 2;
        private readonly int totalCells;
        private int maxnumber;
        private double grainBoundaryEnergy;
        private bool growOnlyOnBoundary;
        private int perStep;

        private Cell[][] cells;
        private HashSet<(int r, int c)> allIndices;
        private HashSet<(int r, int c)> recrystalized;
        private readonly NucleationModule nucleation;

        public Recrystalizer(Cell[][] cells, NucleationModule nucleator, double grainBoundaryEnergy, bool growOnlyOnBoundary, int perStep)
        {
            maxnumber = minnumber;
            this.cells = cells;
            totalCells = cells.Length * cells[0].Length;
            recrystalized = new HashSet<(int r, int c)>();
            allIndices = GetAllIndices(cells);
            this.grainBoundaryEnergy = grainBoundaryEnergy;
            this.growOnlyOnBoundary = growOnlyOnBoundary;
            this.nucleation = nucleator;
            this.perStep = perStep;
        }

        public void ApplyNucleationModule(bool onlyOnBoundary)
        {
            var moore = new MooreNeigbourhood();

            var rows = cells.Length;
            var columns = cells[0].Length;

            List<(int r, int c)> bank;
            if (onlyOnBoundary)
            {
                bank = GetAvailableBoundaryIndices(cells);
            }
            else
            {
                bank = GetAvailableIndices();
            }

            maxnumber = nucleation.Nucleate(bank, maxnumber, recrystalized, cells, perStep);
        }

        public void Generations(int count)
        {
            for (int i = 0; i < count; ++i)
            {
                Generation();
            }
        }

        private void Generation()
        {
            var cellsForIteration = allIndices.ToList();
            var generationRecrystalized = new Dictionary<(int r, int c), Cell>();
            var moore = new MooreNeigbourhood();

            ApplyNucleationModule(growOnlyOnBoundary);
            while (cellsForIteration.Count > 0)
            {
                var (r, c) = cellsForIteration[rand.Next(0, cellsForIteration.Count)];
                cellsForIteration.Remove((r, c));
                var neighbours = moore.GetNeighbours(cells, (r, c));
                if (neighbours.Any(n => n.Id > minnumber))
                {
                    var neigArr = neighbours.Where(n => n.Id > minnumber).ToArray();
                    var neig = neigArr[rand.Next(0, neigArr.Length)];
                    var chosenSite = cells[r][c];

                    var energyBefore = neighbours.Where(cell => cell.Id != chosenSite.Id).Count() * grainBoundaryEnergy + chosenSite.H;
                    var energyAfter = neighbours.Where(cell => cell.Id != neig.Id).Count() * grainBoundaryEnergy;

                    if (energyAfter < energyBefore)
                    {
                        recrystalized.Add((r, c));
                        cells[r][c] = neig;
                    }
                }
            }
        }

        private List<(int r, int c)> GetAvailableBoundaryIndices(Cell[][] cells)
        {
            var moore = new MooreNeigbourhood();

            var rows = cells.Length;
            var columns = cells[0].Length;

            var borders = new HashSet<(int, int)>();

            for (var r = 0; r < rows; ++r)
            {
                for (var c = 0; c < columns; ++c)
                {
                    var neighbours = moore.GetNeighbours(cells, (r, c)).Where(n => n.State == CellState.Occupied);
                    if (neighbours.Select(n => n.Id).Distinct().Count() > 1)
                    {
                        borders.Add((r, c));
                    }
                }
            }

            return borders.Except(recrystalized).ToList();
        }

        private HashSet<(int r, int c)> GetAllIndices(Cell[][] cells)
        {
            var rows = cells.Length;
            var columns = cells[0].Length;

            var all = new HashSet<(int r, int c)>();
            for (var r = 0; r < rows; ++r)
            {
                for (var c = 0; c < columns; ++c)
                {
                    all.Add((r, c));
                }
            }

            return all;
        }

        private List<(int r, int c)> GetAvailableIndices()
        {
            return allIndices.Except(recrystalized).ToList();
        }
    }
}
