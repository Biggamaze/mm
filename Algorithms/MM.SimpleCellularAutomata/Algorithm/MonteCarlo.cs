﻿using MM.CalcParameters.Neighbourhood;
using System;
using System.Linq;

namespace MM.SimpleCellularAutomata.Algorithm
{
    public static class MonteCarlo
    {
        private static Random random = new Random();

        public static void Put(Cell[][] cells, int maximumNumber, int numStates)
        {
            var rows = cells.Length;
            var columns = cells[0].Length;

            for (int r = 0; r < rows; ++r)
            {
                for (int c = 0; c < columns; ++c)
                {
                    if (cells[r][c].State == CellState.Empty)
                    {
                        cells[r][c].Id = random.Next(maximumNumber + 1, maximumNumber + 1 + numStates);
                        cells[r][c].State = CellState.Occupied;
                    }
                }
            }
        }

        public static void Generations(Cell[][] cells, int numGenerations)
        {
            for (int g = 0; g < numGenerations; ++g)
            {
                Generation(cells);
            }
        }

        public static void Generation(Cell[][] cells)
        {
            var moore = new MooreNeigbourhood();

            var indices = cells.SelectMany((x, r) => x.Select((y, c) => (r, c))).ToHashSet();

            while (true)
            {
                if (indices.Count <= 0)
                {
                    break;
                }

                var (r, c) = indices.First();
                indices.Remove((r, c));

                if (cells[r][c].State == CellState.Excluded)
                {
                    continue;
                }

                var neighbours = moore.GetNeighbours(cells, (r, c))
                    .Where(cell => cell.State == CellState.Occupied)
                    .ToArray();

                var kroneckerDelta = 0;
                for (int i = 0; i < neighbours.Length; ++i)
                {
                    if (neighbours[i].Id != cells[r][c].Id)
                    {
                        kroneckerDelta++;
                    }
                }
                var energyBefore = kroneckerDelta;

                var energyAfter = 0;
                var proposedState = neighbours[random.Next(0, neighbours.Length)];
                for (int i = 0; i < neighbours.Length; ++i)
                {
                    if (neighbours[i].Id != proposedState.Id)
                    {
                        energyAfter++;
                    }
                }

                if (energyAfter <= energyBefore)
                {
                    cells[r][c].Id = proposedState.Id;
                }
            }
        }
    }
}
