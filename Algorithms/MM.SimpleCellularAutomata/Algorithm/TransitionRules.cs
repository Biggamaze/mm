﻿using MM.CalcParameters.Neighbourhood;
using MM.SimpleCellularAutomata;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MM.CalcParameters
{
    public static class TransitionRules
    {
        private static readonly MooreNeigbourhood moore = new MooreNeigbourhood();
        private static readonly VonNeumannNeighbourhood vonNeumann = new VonNeumannNeighbourhood();
        private static readonly FurtherMooreNeighbourhood furtherMoore = new FurtherMooreNeighbourhood();
        private static readonly Random random = new Random();

        public static int TranstionRule(Cell[][] cells, int row, int column, int rule4probability)
        {
            var mooreN = moore.GetNeighbours(cells,(row, column));

            var vonNeumannNeigs = new Cell[] { mooreN[1], mooreN[3], mooreN[4], mooreN[6] }
            .Where(e => e.State != CellState.Excluded).ToArray();

            var furtherMoore = new Cell[] { mooreN[0], mooreN[2], mooreN[5], mooreN[7] }
            .Where(e => e.State != CellState.Excluded).ToArray();

            mooreN = mooreN.Where(e => e.State != CellState.Excluded).ToArray();

            var r1 = Rule1(mooreN);
            if (r1 != 0)
            {
                return r1;
            }

            var r2 = Rule2(vonNeumannNeigs);
            if (r2 != 0)
            {
                return r2;
            }

            var r3 = Rule3(furtherMoore);
            if (r3 != 0)
            {
                return r3;
            }

            return Rule4(mooreN, rule4probability);
        }

        public static int Rule1(Cell[] mooreNeigs)
        {
            return mooreNeigs.NewValueWithThreshold(5);
        }

        public static int Rule2(Cell[] vonNeum)
        {
            return vonNeum.NewValueWithThreshold(3);
        }

        public static int Rule3(Cell[] furtherMooreNeigs)
        {
            return furtherMooreNeigs.NewValueWithThreshold(3);
        }

        public static int Rule4(Cell[] mooreN, int probability)
        {
            if (random.Next(1,101) <= probability)
            {
                return mooreN.GetMostFrequent();
            }

            return -1;
        }

        private static int NewValueWithThreshold(this Cell[] cells, int threshold)
        {
            var occurences = cells.GetFrequencies();

            foreach (var kv in occurences)
            {
                if (kv.Value >= threshold)
                {
                    return kv.Key;
                }
            }

            return 0;
        }

        private static int GetMostFrequent(this Cell[] cells)
        {
            var freqs = cells.GetFrequencies();
            var max = 0;

            foreach (var val in freqs)
            {
                if (val.Value > max)
                {
                    max = val.Key;
                }
            }

            return max;
        }

        private static Dictionary<int, int> GetFrequencies(this Cell[] cells)
        {
            var occurences = new Dictionary<int, int>
            {
                { 0, -1 }
            };

            for (int i = 0; i < cells.Length; ++i)
            {
                if (cells[i].State != CellState.Occupied)
                {
                    continue;
                }

                if (!occurences.ContainsKey(cells[i].Id))
                {
                    occurences.Add(cells[i].Id, 0);
                }
                occurences[cells[i].Id]++;
            }

            return occurences;
        }
    }
}
