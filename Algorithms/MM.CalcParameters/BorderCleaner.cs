﻿using MM.CalcParameters.Neighbourhood;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MM.CalcParameters.Cleaners
{
    public static class BorderCleaner
    {
        public static void DrawAllBoundaries(int[][] cells)
        {
            var rows = cells.Length;
            var columns = cells[0].Length;
            var neigbourhood = new MooreNeigbourhood();

            var changes = new List<((int r, int c) cds, int val)>();
            for (var r = 0; r < rows; ++r)
            {
                for (var c = 0; c < rows; ++c)
                {
                    if (cells[r][c] == -1 )
                    {
                        continue;
                    }

                    var ngs = neigbourhood.GetNeighbours(cells, (r, c)).Where(n => n > 0);
                    var nGroups = ngs.GroupBy(e => e).Count();

                    if (nGroups > 1)
                    {
                        changes.Add(((r, c), -1));
                    }
                    else
                    {
                        changes.Add(((r, c), 0));
                    }
                }
            }

            foreach (var (cds, val) in changes)
            {
                cells[cds.r][cds.c] = val;
            }
        }

        //public static void DrawSelectedBoundaries<T>(T[,] input, IEnumerable<int> cols) where T : struct, IPoint
        //{
        //    var xLen = input.GetLength(0);
        //    var yLen = input.GetLength(1);
        //    var neigbourhood = new MooreNeigbourhood<T>();

        //    var intermediate = input.DeepCopy2D();
        //    for (var row = 0; row < yLen; ++row)
        //    {
        //        for (var col = 0; col < xLen; ++col)
        //        {
        //            var neigs = neigbourhood.GetNeighbours(intermediate, (row, col))
        //                .Where(n => n.Type == GrainType.RegularInitialized || n.Type == GrainType.Fixed);

        //            if (intermediate[row, col].Type == GrainType.Inclusions)
        //            {
        //                continue;
        //            }

        //            if (!cols.Any(e => e == intermediate[row, col].Value))
        //            {
        //                input[row, col].Value = 0;
        //                input[row, col].Type = GrainType.Undefined;
        //                continue;
        //            }

        //            var nGroups = neigs.GroupBy(e => e.Value).Count();

        //            if (nGroups > 1)
        //            {
        //                input[row, col].Value = -1;
        //                input[row, col].Type = GrainType.Inclusions;
        //            }
        //            else
        //            {
        //                input[row, col].Value = 0;
        //            }
        //        }
        //    }
        //}
    }
}
