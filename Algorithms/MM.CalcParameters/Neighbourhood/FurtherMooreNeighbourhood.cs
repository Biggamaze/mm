﻿namespace MM.CalcParameters.Neighbourhood
{
    public class FurtherMooreNeighbourhood : Neighbourhood
    {
        protected override (int row, int column)[] GetIndices((int row, int column) cds)
        {
            return new[] 
            {
                (cds.row - 1, cds.column - 1),
                (cds.row - 1, cds.column + 1),
                (cds.row + 1, cds.column - 1),
                (cds.row + 1, cds.column + 1),
            };
        }
    }
}
