﻿using System.Collections.Generic;
using System.Linq;

namespace MM.CalcParameters.Neighbourhood
{
    public abstract class Neighbourhood 
    {
        public int[] GetNeighbours(int[][] array, (int row, int column) coords)
        {
            int rows = array.Length;
            int columns = array[0].Length;

            var ind = GetIndices(coords);
            var vals = new int[ind.Length];

            for (int i = 0; i < ind.Length; ++i)
            {
                var row = ind[i].row >= 0 ? (ind[i].row < rows ? ind[i].row : rows - ind[i].row) : rows + ind[i].row;
                var column = ind[i].column >= 0 ? (ind[i].column < columns ? ind[i].column : columns - ind[i].column) : columns + ind[i].column;

                vals[i] = array[row][column];
            }

            return vals;
        }

        protected abstract (int row, int column)[] GetIndices((int row, int column) cds);
    }
}
