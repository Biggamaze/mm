﻿using System;

namespace MM.CalcParameters.Inclusions
{
    public class CircularInclusion : Inclusion
    {
        protected override void Put(Cell[][] cells, int radius, (int row, int column) center)
        {
            for (var r = center.row - radius; r < center.row + radius; r++)
            {
                for (var c = center.column - radius; c < center.column + radius; c++)
                {
                    var dist = Math.Sqrt(Math.Pow(center.row - r, 2) + Math.Pow(center.column - c, 2));
                    if (dist <= radius)
                    {
                        cells[r][c] = -1;
                    }
                }
            }
        }
    }
}
