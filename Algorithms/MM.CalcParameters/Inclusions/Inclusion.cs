﻿namespace MM.CalcParameters.Inclusions
{
    public abstract class Inclusion
    {
        protected abstract void Put(int[][] cells, int radius, (int row, int column) center);

        public bool TryPut(int[][] cells, int radius, (int row, int column) center)
        {
            var (rows, columns) = (cells.Length, cells[0].Length);

            if (!IsInBounds(center, rows, columns, radius))
            {
                return false;
            }

            if (HasNeighbouringInclusions(center, cells, radius))
            {
                return false;
            }

            Put(cells, radius, center);
            return true;
        }

        private bool IsInBounds((int row, int column) cell, int rows, int columns, int radius)
        {
            var minRow = cell.row - radius;
            var maxRow = cell.row + radius;
            var minCol = cell.column - radius;
            var maxCol = cell.column + radius;

            return (minRow >= 0 && minCol >= 0
                    && maxRow < rows && maxCol < columns);
        }

        private bool HasNeighbouringInclusions((int row, int column) cell, int[][] cells, int radius)
        {
            for (var r = cell.row - radius; r < cell.row + radius; r++)
            {
                for (var c = cell.column - radius; c < cell.column + radius; c++)
                {
                    if (cells[r][c] < 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
