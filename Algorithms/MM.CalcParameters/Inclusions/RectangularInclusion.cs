﻿namespace MM.CalcParameters.Inclusions
{
    public class RectangularInclusion : Inclusion
    {
        protected override void Put(int[][] cells, int radius, (int row, int column) center)
        {
            for (var r = center.row - radius; r < center.row + radius; r++)
            {
                for (var c = center.column - radius; c < center.column + radius; c++)
                {
                    cells[r][c] = -1;
                }
            }
        }
    }
}
