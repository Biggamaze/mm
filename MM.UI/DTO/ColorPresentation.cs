﻿using System.Windows.Media;

namespace MM.UI.DTO
{ 
    public class ColorPresentation
    {
        public int Value { get; set; }
        public SolidColorBrush Color { get; set; }
    }
}
