﻿using Microsoft.Win32;
using MM.CalcParameters;
using MM.CalcParameters.Cleaners;
using MM.CalcParameters.Inclusions;
using MM.GUI.ImageProcessing.ImportExport;
using MM.SimpleCellularAutomata;
using MM.SimpleCellularAutomata.Algorithm;
using MM.SimpleCellularAutomata.Recrystalization;
using MM.SimpleCellularAutomata.Recrystalization.NucleationModules;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;

namespace MM.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private Dictionary<string, Inclusion> incs;
        private Dictionary<string, Func<int, int>> structures;
        private Dictionary<string, NucleationModule> nucleationModules;
        private Dictionary<string, bool> growOnlyOnBoundaries;
        private CellSpace CellSpace { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string name = "def")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private bool _isSpaceInitialized;
        public bool IsSpaceInitialized
        {
            get { return _isSpaceInitialized; }
            set { _isSpaceInitialized = value; OnPropertyChanged(nameof(IsSpaceInitialized)); OnPropertyChanged(nameof(IsSpaceUninitialized)); }
        }

        public bool IsSpaceUninitialized
        {
            get { return !_isSpaceInitialized; }
        }

        public MainWindow()
        {
            InitializeComponent();
            PopulateInclusionTypes();
            PopulateStructure();
            PopulateRecrystalizationLocation();
            PopulateNucleationModules();
            IsSpaceInitialized = false;
        }

        private void PopulateInclusionTypes()
        {
            incs = new Dictionary<string, Inclusion>
            {
                {"Rectangular", new RectangularInclusion() },
                {"Circular", new CircularInclusion() }
            };

            InclTypeCmb.ItemsSource = incs.Keys;
            InclTypeCmb.SelectedItem = incs.Keys.First();
        }

        private void PopulateStructure()
        {
            structures = new Dictionary<string, Func<int, int>>
            {
                {"Substructure", Structure.Substructure },
                {"DualPhase", Structure.DualPhase }
            };

            StructureCmbx.ItemsSource = structures.Keys;
            StructureCmbx.SelectedItem = structures.Keys.First();
        }

        private void PopulateNucleationModules()
        {
            nucleationModules = new Dictionary<string, NucleationModule>
            {
                {"One shot", new OneTimeNucleationModule() },
                {"Constant", new ConstantNucleationModule() },
                {"Increasing", new IncresingNucleationModule() }
            };

            NucleationModuleCbx.ItemsSource = nucleationModules.Keys;
            NucleationModuleCbx.SelectedItem = nucleationModules.Keys.First();
        }

        private void PopulateRecrystalizationLocation()
        {
            growOnlyOnBoundaries = new Dictionary<string, bool>
            {
                {"Boundaries", true },
                {"Anywhere", false }
            };

            NucleonsLocationCbx.ItemsSource = growOnlyOnBoundaries.Keys;
            NucleonsLocationCbx.SelectedItem = growOnlyOnBoundaries.Keys.First();
        }

        private void SetSpace_Click(object sender, RoutedEventArgs e)
        {
            if (!(int.TryParse(SizeX.Text, out var columns)
                  && int.TryParse(SizeY.Text, out var rows)))
            {
                MessageBox.Show("Fill in space dimensions and number of cells!", "Warning");
                return;
            }

            if (CellSpace == null)
            {
                CellSpace = new CellSpace(rows, columns);
            }

            var r4p = 50;

            RowNumTBCK.Text = rows.ToString();
            ColNumTBCK.Text = columns.ToString();
            CellSpace.Rule4Probability = r4p;
            Rule4ProbTBCK.Text = r4p.ToString();
        }

        private void SetRule4Probability(object sender, RoutedEventArgs e)
        {
            if (!int.TryParse(rule4Tbx.Text, out var rule4p))
            {
                MessageBox.Show("Fill in rule 4 probability", "Warning");
                return;
            }

            if (CellSpace != null)
            {
                CellSpace.Rule4Probability = rule4p;
                Rule4ProbTBCK.Text = rule4p.ToString();
            }
        }

        private void FinalGeneration_Click(object sender, RoutedEventArgs e)
        {
            var thread = new Thread(() =>
            {
                var res = CellSpace.FinalGeneration();
                CanvasG.DisplayCells(res);
            })
            {
                IsBackground = true
            };
            thread.Start();
        }

        private void NextGeneration_Click(object sender, RoutedEventArgs e)
        {
            if (!int.TryParse(GenerationsCount.Text, out var generations))
            {
                return;
            }

            CalcOneBtn.IsEnabled = false;
            var thread = new Thread(() =>
            {
                for (int i = 0; i < generations; ++i)
                {
                    CellSpace.Generations(1);
                    CanvasG.DisplayCells(CellSpace.Cells);
                }
                Dispatcher.Invoke(() => CalcOneBtn.IsEnabled = true);
            });
            thread.Start();
        }

        private void SetInclusion_Clicked(object sender, RoutedEventArgs e)
        {
            if (!(int.TryParse(InclNoTb.Text, out var inclusionNumber)
                && int.TryParse(InclSizeTb.Text, out var inclusionSize)))
            {
                MessageBox.Show("Fill in inclusion number and inclusion size!", "Warning");
                return;
            }

            CanvasG.DisplayCells(CellSpace.PutInclusions(inclusionNumber, inclusionSize, incs[InclTypeCmb.Text]));
        }

        private void TextExport_Click(object sender, RoutedEventArgs e)
        {
            var dial = new SaveFileDialog
            {
                Filter = "Text file (*.txt)|*.txt"
            };

            if (dial.ShowDialog() == true)
            {
                TextExporterImporter.ExportToTxt(CellSpace.Cells, dial.FileName);
            }
        }

        private void TextImport_Click(object sender, RoutedEventArgs e)
        {
            var dial = new OpenFileDialog()
            {
                Filter = "Text file (*.txt)|*.txt"
            };

            if (dial.ShowDialog() == true)
            {
                CellSpace = new CellSpace(TextExporterImporter.ImportFromTxt(dial.FileName));
                CanvasG.DisplayCells(CellSpace.Cells);
            }
        }

        private void BMPExport_Click(object sender, RoutedEventArgs e)
        {
            var dial = new SaveFileDialog
            {
                Filter = "Png (*.png)|*.png"
            };

            if (dial.ShowDialog() == true)
            {
                BmpExporterImporter.SaveToFile(dial.FileName, CellSpace.Cells);
            }
        }

        private void BMPImport_Click(object sender, RoutedEventArgs e)
        {
            var dial = new OpenFileDialog
            {
                Filter = "Png (*.png)|*.png"
            };

            if (dial.ShowDialog() == true)
            {
                CellSpace = new CellSpace(BmpExporterImporter.ReadFromFile(dial.FileName));
                CanvasG.DisplayCells(CellSpace.Cells);
            }
        }

        private void SetStructure_Clicked(object sender, RoutedEventArgs e)
        {
            var cw = new ColorPickerWindow(CellSpace.Cells);
            cw.ColorsPicked += (exceptForColors) => CellSpace.Clean(exceptForColors);
            cw.Show();
            cw.Closed += (s, a) =>
            {
                var strct = structures[(string)StructureCmbx.SelectedValue];
                CellSpace.SetStructure(strct);
                CanvasG.DisplayCells(CellSpace.Cells);
                IsSpaceInitialized = false;
            };
        }

        private void UnsetStructure_Clicked(object sender, RoutedEventArgs e)
        {
            CellSpace.UnsetStructure();
        }

        private void DrawBorder_Clicked(object sender, RoutedEventArgs e)
        {
            var cw = new ColorPickerWindow(CellSpace.Cells);
            cw.ColorsPicked += (cols) => BorderCleaner.DrawAllBoundaries(CellSpace.Cells);
            cw.Show();
            cw.Closed += (s, a) => CanvasG.DisplayCells(CellSpace.Cells);
            IsSpaceInitialized = false;
        }

        private void Number_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            var regex = new Regex(@"[\d{1,},{1}]");
            e.Handled = !regex.IsMatch(e.Text);
        }

        private void ClearBtn_Click(object sender, RoutedEventArgs e)
        {
            CellSpace = null;
            CanvasG.Grains.Source = null;
            IsSpaceInitialized = false;
        }

        private void SetCellsCA_Clicked(object sender, RoutedEventArgs e)
        {
            if (!int.TryParse(NumCellsCA.Text, out var numCells))
            {
                MessageBox.Show("Provide correct number of cells");
            }

            CellSpace.PutCellsCA(numCells);
            CanvasG.DisplayCells(CellSpace.Cells);
            IsSpaceInitialized = true;
        }

        private void SetCellsMC_Clicked(object sender, RoutedEventArgs e)
        {
            if (!int.TryParse(NumStatesMC.Text, out var numCells))
            {
                MessageBox.Show("Provide correct number of cells");
            }

            CellSpace.PutCellsMC(numCells);
            CanvasG.DisplayCells(CellSpace.Cells);
            IsSpaceInitialized = true;
        }

        private void DistributeEnergy_Click(object sender, RoutedEventArgs e)
        {
            if (!int.TryParse(GrainEdgeEnergy.Text, out var edgeEnergy)
                || !int.TryParse(EnergyInsideGrains.Text, out var energyInside)
                || !double.TryParse(GrainBoundaryEnergy.Text, out var boundaryEnergy)
                || !int.TryParse(SRXPerStep.Text, out var srxPerStep))
            {
                MessageBox.Show("Pleas input proper values of energy");
                return;
            }

            if (CellSpace == null)
            {
                return;
            }

            EnergyDistribution.Distribute(CellSpace.Cells, edgeEnergy, energyInside);

            var nuclator = nucleationModules[(string)NucleationModuleCbx.SelectedItem];
            nuclator.Reset();
            var onlyOnBoundaries = growOnlyOnBoundaries[(string)NucleonsLocationCbx.SelectedItem];

            var recrystalizer = new Recrystalizer(CellSpace.Cells, nuclator, boundaryEnergy, onlyOnBoundaries, srxPerStep);
            CellSpace.Recrystalization = recrystalizer;

            CanvasG.DisplayEnergy(CellSpace.Cells);
        }

        private void NucleonsView_Click(object sender, RoutedEventArgs e)
        {
            if (CellSpace?.Cells == null)
            {
                return;
            }

            CanvasG.DisplayCells(CellSpace.Cells);
        }

        private void EnergyView_Click(object sender, RoutedEventArgs e)
        {
            if (CellSpace?.Cells == null)
            {
                return;
            }

            CanvasG.DisplayEnergy(CellSpace.Cells);
        }

        private void SwitchToRecrystalization_Clicked(object sender, RoutedEventArgs e)
        {
            CellSpace.CurrentAlgorithm = AutomataAlgorithm.Recrystalization;
        }
    }
}
