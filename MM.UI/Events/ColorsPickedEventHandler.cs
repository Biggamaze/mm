﻿using System.Collections.Generic;

namespace MM.Interfaces.Events
{
    public delegate void ColorsPickedEventHandler(IEnumerable<int> colors);
}
