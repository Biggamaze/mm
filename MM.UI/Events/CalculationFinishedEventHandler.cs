﻿namespace MM.Interfaces.Events
{
    public delegate void CalculationFinishedEventHandler<T>(T[,] output);
}
