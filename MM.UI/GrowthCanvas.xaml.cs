﻿using System.Windows.Controls;
using MM.GUI.ImageProcessing;
using MM.SimpleCellularAutomata;

namespace MM.UI
{
    /// <summary>
    /// Interaction logic for GrowthCanvas.xaml
    /// </summary>
    public partial class GrowthCanvas : UserControl
    {
        public GrowthCanvas()
        {
            InitializeComponent();
        }

        public void DisplayCells(Cell[][] res)
        {
            Dispatcher.Invoke(() =>
            {
                var wbitmap = GrainArrayProcessor.ToImageSource(res, cell => cell.Id);
                Grains.Source = wbitmap;
            });
        }

        public void DisplayEnergy(Cell[][] cells)
        {
            Dispatcher.Invoke(() =>
            {
                var wbitmap = GrainArrayProcessor.ToImageSource(cells, cell => cell.H);
                Grains.Source = wbitmap;
            });
        }
    }
}
