﻿using MM.Interfaces.Events;
using MM.UI.DTO;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using MM.GUI.ImageProcessing;
using MM.SimpleCellularAutomata;

namespace MM.UI
{
    /// <summary>
    /// Interaction logic for ColorPickerWindow.xaml
    /// </summary>
    public partial class ColorPickerWindow : Window
    {
        public event ColorsPickedEventHandler ColorsPicked;

        public ColorPickerWindow(Cell[][] cells)
        {
            InitializeComponent();
            PopulateListBox(cells);
        }

        private void PopulateListBox(Cell[][] cells)
        {
            var dist = cells.SelectMany(x => x.Distinct()).Distinct();

            ColorLb.ItemsSource = dist
                .Where(x => x.State == CellState.Occupied)
                .OrderBy(x => x.Id)
                .Select(x => new ColorPresentation
                {
                    Value = x.Id,
                    Color = new SolidColorBrush(Color.FromRgb(ColorPalette.GetRgb(x.Id)[2], ColorPalette.GetRgb(x.Id)[1], ColorPalette.GetRgb(x.Id)[0]))
                });
        }

        private void ApplyClicked(object sender, RoutedEventArgs e)
        {
            var colNums = ColorLb.SelectedItems.Cast<ColorPresentation>().Select(x => x.Value);
            ColorsPicked?.Invoke(colNums);
            Close();
        }

        private void SelectAll_Clicked(object sender, RoutedEventArgs e)
        {
            ColorLb.SelectAll();
            var colNums = ColorLb.SelectedItems.Cast<ColorPresentation>().Select(x => x.Value);
            ColorsPicked?.Invoke(colNums);
            Close();
        }
    }
}
